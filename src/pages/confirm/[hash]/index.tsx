import Head from "next/head";
import ConfirmHash from "./ConfirmHash";

const Index = () => (
  <>
    <Head>
      <link rel="icon" href="/favicon.ico" />
      <title>Confirm</title>
    </Head>
    <ConfirmHash />
  </>
);

export default Index;
