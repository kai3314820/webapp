import { Box, Spinner, Text, VStack } from "@chakra-ui/react";
import React from "react";
import { api } from "~/utils/api";
import { useEffect } from "react";
import { useRouter } from "next/router";

export default function ConfirmHash() {
  const confirm = api.waitlist.confirm.useMutation();
  const router = useRouter();
  const { hash } = router.query;

  useEffect(() => {
    if (hash) {
      confirm.mutate({ token: String(hash) });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [hash]);

  return (
    <>
      <Box h="100vh" bg="gray.200">
        <VStack spacing={12} pt={24}>
          <Box p={10} bg="white" color="black" w={["360px", "600px", "700px"]}>
            <VStack spacing={8} align="left">
              {confirm.isLoading && <Spinner alignSelf={"center"} />}
              {confirm.isSuccess && (
                <>
                  <Text fontWeight="700">Success! 🥳</Text>
                  <Text>
                    Your e-mail address ({confirm.data?.updatedUser.email}) has
                    been verified and you have successfully subscribed.
                  </Text>
                </>
              )}
              {confirm.error?.data?.code === "CONFLICT" && (
                <>
                  <Text fontWeight="700">Hello again! 🤗</Text>
                  <Text>
                    The e-mail address {confirm.data?.updatedUser.email} has
                    already been verified. You’re all set!
                  </Text>
                </>
              )}
              {confirm.error?.data?.code === "NOT_FOUND" && (
                <Text>Wrong validation code.</Text>
              )}
              {confirm.error?.data?.code === "BAD_REQUEST" && (
                <Text>Invalid validation code.</Text>
              )}
            </VStack>
          </Box>
          <Text textColor="gray.700">© BTBverse.xyz</Text>
        </VStack>
      </Box>
    </>
  );
}
