import { Heading } from "@chakra-ui/react";
import Head from "next/head";
import WebAppLayout from "~/components/Layout/WebAppLayout";

const Index = () => {
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>Utility controller</title>
      </Head>
      <WebAppLayout>
        <Heading>Utility controller</Heading>
      </WebAppLayout>
    </>
  );
};

export default Index;
