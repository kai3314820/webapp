import { Heading } from "@chakra-ui/react";
import Head from "next/head";
import WebAppLayout from "~/components/Layout/WebAppLayout";

const Index = () => {
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>Later</title>
      </Head>
      <WebAppLayout>
        <Heading>🚧 Come back later 🚧</Heading>
      </WebAppLayout>
    </>
  );
};

export default Index;
