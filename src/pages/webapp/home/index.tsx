import { Heading } from "@chakra-ui/react";
import Head from "next/head";
import WebAppLayout from "~/components/Layout/WebAppLayout";

const Index = () => {
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>Home</title>
      </Head>
      <WebAppLayout>
        <Heading>Home</Heading>
      </WebAppLayout>
    </>
  );
};

export default Index;
