import { Heading } from "@chakra-ui/react";
import Head from "next/head";
import WebAppLayout from "~/components/Layout/WebAppLayout";

const Index = () => {
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>WebApp</title>
      </Head>
      <WebAppLayout>
        <Heading>Welcome</Heading>
      </WebAppLayout>
    </>
  );
};

export default Index;
