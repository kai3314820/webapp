import {
  VStack,
  Button,
  HStack,
  useToast,
  Table,
  TableCaption,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  Avatar,
  Spinner,
} from "@chakra-ui/react";
import { signIn, useSession } from "next-auth/react";
import Head from "next/head";
import { api } from "~/utils/api";
import { useEffect, useState } from "react";
import { type User } from "@prisma/client";
import {
  CheckIcon,
  CloseIcon,
  TriangleDownIcon,
  TriangleUpIcon,
} from "@chakra-ui/icons";
import { useRouter } from "next/router";
import WebAppLayout from "~/components/Layout/WebAppLayout";

const Index = () => {
  const { data: session, status } = useSession();
  const [users, setUsers] = useState<User[] | undefined>([]);
  const query = api.account.getUsers.useQuery();
  const mutation = api.account.promote.useMutation();
  const degrade = api.account.degrade.useMutation();
  const toast = useToast();
  const router = useRouter();

  useEffect(() => {
    setUsers(query.data);
  }, [query.data]);

  if (status === "unauthenticated") {
    void signIn();
  }

  if (status === "authenticated") {
    if (!session.user.isAdmin) {
      void router.push("/");
    }
  }

  const handlePromote = async (userEmail: string | null) => {
    if (userEmail) {
      const updatedUsers = await mutation.mutateAsync({ email: userEmail });
      if (updatedUsers) {
        setUsers(updatedUsers);

        toast({
          title: "User promoted",
          status: "success",
          duration: 2000,
          isClosable: true,
        });
      }
    }
  };

  const handleDegrade = async (userEmail: string | null) => {
    if (userEmail) {
      const users = await degrade.mutateAsync({ email: userEmail });
      if (users) {
        setUsers(users);
        toast({
          title: "User degraded",
          status: "success",
          duration: 2000,
          isClosable: true,
        });
      }
    }
  };

  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>Users</title>
      </Head>
      {session?.user.isAdmin && (
        <WebAppLayout>
          <VStack>
            {!session ? (
              <Spinner />
            ) : (
              <TableContainer w="100%">
                <Table size="sm" variant="simple">
                  <TableCaption>Users</TableCaption>
                  <Thead>
                    <Tr>
                      <Th>E-Mail</Th>
                      <Th>Image</Th>
                      <Th>Admin</Th>
                      <Th></Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {users &&
                      users.map((user) => (
                        <Tr key={user.id}>
                          <Td>{user.email}</Td>
                          <Td>
                            {user.image && (
                              <Avatar size="md" src={user.image}></Avatar>
                            )}
                            {!user.image && user.name && (
                              <Avatar name={user.name}></Avatar>
                            )}
                          </Td>
                          <Td>
                            {user.isAdmin ? (
                              <CheckIcon color="green" />
                            ) : (
                              <CloseIcon boxSize="3" color="red" />
                            )}
                          </Td>
                          <Td>
                            <HStack>
                              {!user.emailVerified && (
                                <Button
                                  // eslint-disable-next-line @typescript-eslint/no-misused-promises
                                  onClick={() => handlePromote(user.email)}
                                  colorScheme="blue"
                                  size="xs"
                                >
                                  <TriangleUpIcon boxSize={3} mr={1} />
                                  Promote
                                </Button>
                              )}
                              <Button
                                // eslint-disable-next-line @typescript-eslint/no-misused-promises
                                onClick={() => handleDegrade(user.email)}
                                colorScheme="red"
                                size="xs"
                              >
                                <TriangleDownIcon boxSize={3} mr={1} />
                                Degrade
                              </Button>
                            </HStack>
                          </Td>
                        </Tr>
                      ))}
                  </Tbody>
                </Table>
              </TableContainer>
            )}
          </VStack>
        </WebAppLayout>
      )}
    </>
  );
};

export default Index;
