import {
  VStack,
  Text,
  Button,
  HStack,
  useToast,
  Table,
  TableCaption,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  Spinner,
} from "@chakra-ui/react";
import { signIn, useSession } from "next-auth/react";
import Head from "next/head";
import { api } from "~/utils/api";
import { useEffect, useState } from "react";
import { type Waitlist } from "@prisma/client";
import { useRouter } from "next/router";
import WebAppLayout from "~/components/Layout/WebAppLayout";
import { DeleteIcon, EmailIcon } from "@chakra-ui/icons";

const Index = () => {
  const { data: session, status } = useSession();
  const [waitlistUser, setWaitlistUser] = useState<Waitlist[] | undefined>([]);
  const mutation = api.waitlist.delete.useMutation();
  const query = api.waitlist.listAll.useQuery();
  const toast = useToast();
  const router = useRouter();

  useEffect(() => {
    setWaitlistUser(query.data);
  }, [query.data]);

  if (status === "unauthenticated") {
    void signIn();
  }

  if (status === "authenticated") {
    if (!session.user.isAdmin) {
      void router.push("/");
    }
  }

  const handleDelete = async (userEmail: string) => {
    const deletedUser = await mutation.mutateAsync({
      email: userEmail,
    });
    setWaitlistUser(waitlistUser?.filter((user) => user.id !== deletedUser.id));
    toast({
      title: "User deleted",
      description: `${deletedUser.email} removed from waitlist`,
      status: "success",
      duration: 2000,
      isClosable: true,
    });
  };

  const handleInviteAgain = (userEmail: string) => {
    // TOOD
    console.log(userEmail);
    toast({
      title: "🤡 Coming soon 🤡",
      status: "info",
      duration: 4000,
      isClosable: true,
    });
  };

  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>Waitlist</title>
      </Head>
      <WebAppLayout>
        <VStack>
          {!session?.user.isAdmin ? (
            <Spinner />
          ) : (
            <TableContainer w="100%">
              <Table variant="simple">
                <TableCaption>Waitlist User</TableCaption>
                <Thead>
                  <Tr>
                    <Th>E-Mail</Th>
                    <Th>Verified</Th>
                    <Th></Th>
                  </Tr>
                </Thead>
                <Tbody>
                  {waitlistUser &&
                    waitlistUser.map((user) => (
                      <Tr key={user.id}>
                        <Td>{user.email}</Td>
                        <Td>
                          {user.emailVerified ? (
                            <Text>
                              {user.emailVerified.toLocaleString("de")}
                            </Text>
                          ) : (
                            <>
                              <HStack>
                                <Spinner size="sm" />
                                <Text fontSize="sm" fontStyle="italic">
                                  waiting for approval ...
                                </Text>
                              </HStack>
                            </>
                          )}
                        </Td>
                        <Td>
                          <HStack>
                            <Button
                              // eslint-disable-next-line @typescript-eslint/no-misused-promises
                              onClick={() => handleDelete(user.email)}
                              size="sm"
                              variant="outline"
                            >
                              <DeleteIcon boxSize={4} />
                            </Button>
                            {!user.emailVerified && (
                              <Button
                                onClick={() => handleInviteAgain(user.email)}
                                variant="outline"
                                size="sm"
                              >
                                <EmailIcon boxSize={4} />
                              </Button>
                            )}
                          </HStack>
                        </Td>
                      </Tr>
                    ))}
                </Tbody>
              </Table>
            </TableContainer>
          )}
        </VStack>
      </WebAppLayout>
    </>
  );
};

export default Index;
