import Head from "next/head";
import {
  Text,
  Spacer,
  VStack,
  Box,
  Spinner,
  Avatar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Flex,
  Badge,
  Divider,
  HStack,
} from "@chakra-ui/react";
import { signIn, useSession } from "next-auth/react";
import WebAppLayout from "~/components/Layout/WebAppLayout";

const Index = () => {
  const { data: session, status } = useSession();

  if (status === "unauthenticated") {
    void signIn();
  }

  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>Account</title>
      </Head>
      <WebAppLayout>
        <VStack>
          {!session ? (
            <Spinner />
          ) : (
            <>
              <Card w={["xxs", "sm", "sm", "md", "lg"]}>
                <CardHeader>
                  <Flex>
                    {session?.user.image && (
                      <Avatar name="Segun Adebayo" src={session.user.image} />
                    )}
                    <Box ml="3">
                      <Text fontWeight="bold">
                        {session && session.user.name}
                        <Badge ml="1" colorScheme="purple">
                          {session.user.isAdmin ? "Admin" : "User"}
                        </Badge>
                      </Text>
                      <Text fontSize="sm">{session && session.user.email}</Text>
                    </Box>
                  </Flex>
                </CardHeader>
                <Divider />
                <CardBody>
                  <VStack>
                    <HStack justify="space-between" w="100%">
                      <Text>Name: </Text>
                      <Text>{session && session.user.name}</Text>
                    </HStack>
                    <HStack justify="space-between" w="100%">
                      <Text>Status: </Text>
                      <Text> {session.user.isAdmin ? "Admin" : "User"}</Text>
                    </HStack>
                    <HStack justify="space-between" w="100%">
                      <Text>E-Mail: </Text>
                      <Text>{session && session.user.email}</Text>
                    </HStack>

                    <HStack justify="space-between" w="100%">
                      <Text>ID: </Text>
                      <Text>{session && session.user.id}</Text>
                    </HStack>
                  </VStack>
                </CardBody>

                <CardFooter
                  justify="space-between"
                  flexWrap="wrap"
                  sx={{
                    "& > button": {
                      minW: "136px",
                    },
                  }}
                ></CardFooter>
              </Card>
            </>
          )}
        </VStack>
        <Spacer pt={24} />
      </WebAppLayout>
    </>
  );
};

export default Index;
