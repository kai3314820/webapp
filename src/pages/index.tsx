import { type NextPage } from "next";
import Head from "next/head";

// import { api } from "~/utils/api";
import Home from "~/components/Home/Home";
import DefaultLayout from "~/components/Layout/DefaultLayout";

const Index: NextPage = () => {
  return (
    <>
      <Head>
        <title>BTBverse</title>
        <meta name="description" content="Unleash the world's creativity" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <DefaultLayout>
        <Home />
      </DefaultLayout>
    </>
  );
};

export default Index;
