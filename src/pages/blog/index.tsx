import Blog from "./Blog";
import Head from "next/head";
import DefaultLayout from "~/components/Layout/DefaultLayout";

const Index = () => (
  <>
    <Head>
      <link rel="icon" href="/favicon.ico" />
      <title>Blog</title>
    </Head>
    <DefaultLayout>
      <Blog />
    </DefaultLayout>
  </>
);

export default Index;
