import { motion } from "framer-motion";
import { useRouter } from "next/router";
import React from "react";
import { BlogContent } from "../../../components/Blog/BlogContent";

export default function BlogPost() {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      {id ? (
        <motion.div
          initial="hidden"
          animate={{
            opacity: [0, 1],
          }}
          transition={{
            type: "spring",
            duration: 0.3,
            repeat: 0,
          }}
        >
          <BlogContent blog_id={id} />
        </motion.div>
      ) : (
        <></>
      )}
    </>
  );
}
