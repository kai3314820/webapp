import Head from "next/head";
import DefaultLayout from "~/components/Layout/DefaultLayout";
import BlogPost from "./BlogPost";

const Index = () => (
  <>
    <Head>
      <title>Blogpost</title>
    </Head>
    <DefaultLayout>
      <BlogPost />
    </DefaultLayout>
  </>
);

export default Index;
