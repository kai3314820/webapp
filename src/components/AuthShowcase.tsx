import {
  Avatar,
  Button,
  HStack,
  Menu,
  MenuButton,
  MenuDivider,
  MenuItem,
  MenuList,
} from "@chakra-ui/react";
import { useSession, signOut } from "next-auth/react";
import Link from "next/link";

export const AuthShowcase: React.FC = () => {
  const { data: session } = useSession();

  return (
    <HStack zIndex={999}>
      {session && (
        <Menu>
          <MenuButton as={Button} variant="link">
            {session.user.image === null ||
            (session.user.image === undefined && session.user.name) ? (
              <Avatar
                bg="gray.500"
                size="md"
                name={session.user.name || undefined}
              />
            ) : (
              <Avatar
                bg="gray.500"
                size="sm"
                name={session.user.name || undefined}
                src={session?.user?.image}
              />
            )}
          </MenuButton>
          <MenuList>
            <MenuItem as={Link} href="/webapp/admin/account">
              Me
            </MenuItem>
            <MenuDivider />
            <MenuItem as={Link} href="/webapp">
              WebApp
            </MenuItem>
            {session.user.isAdmin && (
              <MenuItem as={Link} href="/webapp/admin/users">
                Users
              </MenuItem>
            )}
            {session.user.isAdmin && (
              <MenuItem as={Link} href="/webapp/admin/waitlist">
                Waitlist
              </MenuItem>
            )}
            <MenuDivider />
            <MenuItem as={Link} href="/">
              Website
            </MenuItem>
            <MenuItem as={Link} href="/blog">
              Blog
            </MenuItem>
            <MenuItem as={Link} href="/imprint">
              Imprint
            </MenuItem>
            <MenuItem onClick={() => void signOut()}>Logout</MenuItem>
          </MenuList>
        </Menu>
      )}
    </HStack>
  );
};
