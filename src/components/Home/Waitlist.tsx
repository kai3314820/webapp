/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { EmailIcon } from "@chakra-ui/icons";
import {
  Button,
  Collapse,
  Input,
  InputGroup,
  InputLeftElement,
  Spacer,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import { useState, useEffect } from "react";
import { api } from "~/utils/api";
import { AnimatedButton } from "./AnimatedButton";

export const Waitlist = () => {
  const [waitlistMailAddress, setWaitlistMailAddress] = useState("");
  const toast = useToast();
  const mutation = api.waitlist.add.useMutation();

  useEffect(() => {
    // do some checking here to ensure data exist
    if (mutation.data?.waitlistUser.email) {
      // mutate data if you need to
      toast({
        title: "E-Mail sent",
        description: `Please confirm your subscription`,
        status: "success",
        duration: 2000,
        isClosable: true,
      });
    }
    if (mutation.error?.data?.httpStatus === 409) {
      console.log("duplicated");
      toast({
        title: "You're already there 👍",
        description: `You already opted in to the newsletter`,
        status: "info",
        duration: 2000,
        isClosable: true,
      });
    }
  }, [mutation.data, mutation.error, toast]);

  const { isOpen: isWaitlistOpen, onToggle: onWaitlistToggle } =
    useDisclosure();

  const handleClickWaitlist = () => {
    const expression = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
    const expressionTested: boolean = expression.test(waitlistMailAddress);
    if (expressionTested) {
      mutation.mutate({
        email: waitlistMailAddress,
      });
    } else {
      toast({
        title: "Invalid e-mail format",
        status: "error",
        duration: 2000,
        isClosable: true,
      });
    }
  };

  return (
    <>
      <AnimatedButton
        size="lg"
        width="200px"
        text="join waitlist"
        variant="gradient-outline"
        onClick={onWaitlistToggle}
      />
      <Collapse in={isWaitlistOpen} animateOpacity>
        <Spacer p={1} />
        <InputGroup w={["300px", "400px", "500px"]}>
          <InputLeftElement
            pointerEvents="none"
            // eslint-disable-next-line react/no-children-prop
            children={<EmailIcon boxSize={7} pt={2} />}
          />
          <Input
            onChange={(e) => {
              setWaitlistMailAddress(e.target.value);
            }}
            value={waitlistMailAddress}
            onKeyDown={(e) => {
              void (e.key === "Enter" && handleClickWaitlist());
            }}
            size={"lg"}
            placeholder="Email"
            type="email"
          />
          <Button
            ml={2}
            isLoading={mutation.isLoading}
            onClick={() => {
              handleClickWaitlist();
            }}
            size={"lg"}
          >
            Join
          </Button>
        </InputGroup>
      </Collapse>
    </>
  );
};
