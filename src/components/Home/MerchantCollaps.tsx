import {
  Collapse,
  Img,
  Spacer,
  Stack,
  VStack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import { AnimatedButton } from "./AnimatedButton";

export const MerchantCollapse = () => {
  const { isOpen: isDiscoverOpen, onToggle: onDiscoverToggle } =
    useDisclosure();
  return (
    <>
      <Spacer pt={4} />
      <AnimatedButton
        size="lg"
        text={isDiscoverOpen ? "Collapse products" : "Discover products"}
        variant="purple-outline"
        width="200px"
        onClick={onDiscoverToggle}
      />
      <Spacer pt={4} />
      <Collapse in={isDiscoverOpen} animateOpacity>
        <Stack
          spacing={8}
          direction={["column", "column", "column", "column", "row"]}
        >
          <VStack w="350px">
            <Img w="50px" h="50px" src="/icons/megaphone.svg" />
            <Text fontWeight="700" fontSize="20px">
              Campaign Manager
            </Text>
            <Text textAlign="center">
              Design and manage NFT campaigns in the look and feel of your brand
            </Text>
          </VStack>
          <VStack w="350px">
            <Img w="50px" h="50px" src="/icons/controller.svg" />
            <Text fontWeight="700" fontSize="20px">
              Utility Controller
            </Text>
            <Text textAlign="center">
              Enable and control benefits given to NFT holders on your website
              (e.g. discounts, early access, ...)
            </Text>
          </VStack>
          <VStack w="350px">
            <Img w="50px" h="50px" src="/icons/loyalty.svg" />

            <Text fontWeight="700" fontSize="20px">
              NFT Loyalty Program
            </Text>
            <Text textAlign="center">
              Design and run a holistic NFT-based loyalty program (including
              tiers, badges, ...) for your customers
            </Text>
          </VStack>
        </Stack>{" "}
      </Collapse>
    </>
  );
};
