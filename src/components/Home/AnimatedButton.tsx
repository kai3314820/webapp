import { Button } from "@chakra-ui/react";
import { motion } from "framer-motion";

interface IButtonProps {
  text: string;
  variant: string;
  size: string;
  width: string;
  onClick?: () => void;
}

export const AnimatedButton = ({
  text,
  variant,
  size,
  width,
  onClick,
}: IButtonProps) => (
  <motion.div whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }}>
    <Button onClick={onClick} width={width} size={size} variant={variant}>
      {text}
    </Button>
  </motion.div>
);
