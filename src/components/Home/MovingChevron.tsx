import { ChevronDownIcon } from "@chakra-ui/icons";
import { Box, Icon, useColorModeValue } from "@chakra-ui/react";
import { motion } from "framer-motion";

export const MovingChevron = () => (
  <Box cursor="pointer">
    <motion.div
      animate={{ y: [20, -5, 20] }}
      transition={{
        ease: "linear",
        bounce: 100,
        duration: 1,
        repeat: Infinity,
      }}
    >
      <Icon
        as={ChevronDownIcon}
        boxSize={12}
        color={useColorModeValue("black", "white")}
      />
    </motion.div>
  </Box>
);
