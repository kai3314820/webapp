import {
  Text,
  HStack,
  Image,
  VStack,
  Box,
  Link,
  Collapse,
  useDisclosure,
} from "@chakra-ui/react";
import { AnimatedButton } from "./AnimatedButton";

export const Team = () => {
  const { isOpen: isTeamOpen, onToggle: onTeamToggle } = useDisclosure();

  return (
    <>
      <AnimatedButton
        size="lg"
        width="200px"
        text="meet the team"
        variant="purple-outline"
        onClick={onTeamToggle}
      />
      <Box>
        <Collapse in={isTeamOpen} animateOpacity>
          <HStack mb={10} zIndex={-1}>
            <Link
              target="_blank"
              href="https://www.linkedin.com/in/fseeh/"
              rel="noreferrer"
            >
              <VStack spacing={0}>
                <Image
                  placeholder="blur"
                  alt="flo"
                  width={"44"}
                  src={"/crew/flo.svg"}
                />

                <Text fontWeight={800}>Florian</Text>
                <Text fontWeight={150}>Product</Text>
              </VStack>
            </Link>
            <Link
              target="_blank"
              href="https://www.linkedin.com/in/kai-roevenich-1a205786/"
              rel="noreferrer"
            >
              <VStack spacing={0}>
                <Image width={"44"} alt="kai" src={"/crew/kai.svg"} />
                <Text fontWeight={800}>Kai</Text>
                <Text fontWeight={150}>Growth</Text>
              </VStack>
            </Link>
            <Link
              target="_blank"
              href="https://www.linkedin.com/in/peter-seeh-95a324246/"
              rel="noreferrer"
            >
              <VStack spacing={0}>
                <Image width={"44"} alt="peter" src={"/crew/peter.svg"} />
                <Text fontWeight={800}>Peter</Text>
                <Text fontWeight={150}>Tech</Text>
              </VStack>
            </Link>
          </HStack>
        </Collapse>
      </Box>
    </>
  );
};
