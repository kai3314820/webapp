/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { VStack, Spacer, Box } from "@chakra-ui/react";
import React, { useRef } from "react";
import { Logo } from "./Logo";
import { MerchantCollapse } from "./MerchantCollaps";
import { MovingChevron } from "./MovingChevron";
import { RandomPhrase } from "./RandomPhrase";
import { Team } from "./Team";
import TextCard from "./TextCard";
import { Waitlist } from "./Waitlist";

const Home = () => {
  const myRef = useRef(null);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const scrollTo = (ref: any) => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    if (ref && ref.current) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      ref.current.scrollIntoView({ behavior: "smooth", block: "center" });
    }
  };
  return (
    <VStack zIndex="100" overflow="hidden">
      <Box zIndex="100" h="100vh">
        <Spacer pt={[32, 32, 32, 32, 72, 72]} />
        <Logo />
        <VStack>
          <Waitlist />
          <Spacer pt={16} />
          <Box onClick={() => scrollTo(myRef)}>
            <MovingChevron />
          </Box>
        </VStack>
      </Box>
      <Spacer p={96} />
      <Box ref={myRef}>
        <TextCard
          build="now"
          title="Creators"
          paragraph="We strive to enable a more sustainable way for creators to interact with their community and form long-lasting collaborations at eye-level with merchants"
          gradient={{ from: "#6F2DBD", to: "#A663CC " }}
        />
      </Box>
      <Spacer p={96} />
      <Box>
        <VStack>
          <TextCard
            build="now"
            title="Merchants"
            paragraph="We want to create a loyalty and collaboration suite for merchants that want to strengthen their user base and align incentives for users with factors that increase their customer lifetime value"
            gradient={{ from: "#A663CC", to: "#B298DC" }}
          />
          <MerchantCollapse />
        </VStack>
      </Box>
      <Spacer p={96} />
      <Box>
        <TextCard
          build="Later"
          title="Users"
          paragraph="Enabling next-generation collaborations between merchants and creators, we believe we can transform how users travel through the internet and create experiences beyond our current imagination"
          gradient={{ from: "#B298DC", to: "#B8D0EB" }}
        />
      </Box>

      <Spacer p={96} />

      <Box>
        <VStack alignItems={"center"}>
          <TextCard
            title="Unleash the world's creativity"
            paragraph="We strive to become the go-to partner for merchants, creators, and any party on the internet that wants to directly interact and collaborate, breaking free from the rigid, unpredictable, and centralized platforms web2 has created"
            gradient={{ from: "#B8D0EB", to: "#B9FAF8" }}
          />
          <Spacer pt={32} />
          <VStack spacing={5}>
            <Waitlist />
            <Team />
          </VStack>
        </VStack>
      </Box>
      <Spacer p={[10]} />
      <RandomPhrase />
      <Spacer p={[10, 50, 40, 40]} />
    </VStack>
  );
};
export default Home;
