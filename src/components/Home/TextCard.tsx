import { Box, Spacer, Text, VStack } from "@chakra-ui/react";

type TextBoxProps = {
  build?: string;
  title: string;
  paragraph: string;
  gradient: { from: string; to: string };
};

export default function TextBox({
  build,
  title,
  paragraph,
  gradient,
}: TextBoxProps) {
  return (
    <>
      <VStack>
        <Text opacity={0.7} fontWeight={400} fontSize={16}>
          {build}
        </Text>
        <Text
          lineHeight={"110%"}
          align={"center"}
          bgGradient={`linear-gradient(180deg, ${gradient.from} 0%, ${gradient.to} 100%)`}
          bgClip="text"
          fontSize="5xl"
          fontWeight="bold"
          w={["320px", "520px", "820px"]}
        >
          {title}
        </Text>
        <Spacer></Spacer>
        <Box width={"320px"}>
          <Text fontWeight={600} align={"center"}>
            {paragraph}
          </Text>
        </Box>
      </VStack>
    </>
  );
}
