import { Text } from "@chakra-ui/react";
import axios from "axios";
import { type ExtendedRecordMap, type BlockMap } from "notion-types";
import { getPageTitle } from "notion-utils";
import { useEffect, useState } from "react";
import defaultRecordMap from "../Blog/record-map.json";

export const RandomPhrase = () => {
  const [blogPost] = useState<ExtendedRecordMap>(defaultRecordMap);
  const [isLoading, setLoading] = useState(false);

  const page_api =
    "https://notion-api.splitbee.io/v1/page/13606df2c5064dbfa7f8c54826e0f955";

  useEffect(() => {
    setLoading(true);
    axios
      .get(page_api)
      .then((res) => {
        console.log(res.data);
        blogPost.block = res.data as BlockMap;
        setLoading(false);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, [blogPost]);

  return (
    <>
      {!isLoading && <Text textAlign="center" maxW={330} fontStyle="italic">{getPageTitle(blogPost)}</Text>}
    </>
  );
};
