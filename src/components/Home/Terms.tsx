import { Box, Center } from "@chakra-ui/react";
import Link from "next/link";

export const Terms = () => {
  return (
    <>
      <Box position="relative" bottom={1}>
        <Center textColor={"grey"} p={6}>
          <Link href="/imprint">Imprint & Privacy</Link>
        </Center>
      </Box>
    </>
  );
};
