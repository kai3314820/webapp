import { Box, HStack, useColorMode } from "@chakra-ui/react";
import { useSession, signIn } from "next-auth/react";
import { Header, Sidebar } from "../WebApp";

type Props = {
  children: string | JSX.Element | JSX.Element[];
};

export default function WebAppLayout({ children }: Props) {
  const { colorMode } = useColorMode();
  const { data: session, status } = useSession();

  if (status === "unauthenticated") {
    void signIn();
  }

  return (
    <>
      {session?.user && (
        <HStack
          bg={colorMode === "dark" ? "gray.900" : "gray.100"}
          w="100%"
          h="100vh"
          spacing={0}
        >
          <Sidebar />
          <Box w="100%" h="100%" overflowX="hidden">
            <Header />
            <Box
              p={12}
              h="calc(100vh-72px)"
              bg={colorMode === "dark" ? "gray.900" : "gray.100"}
            >
              {children}
            </Box>
          </Box>
        </HStack>
      )}
    </>
  );
}
