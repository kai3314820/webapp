import { Box } from "@chakra-ui/react";
import { Header } from "../Header";
import { Background } from "../Home/Background";
import { Terms } from "../Home/Terms";

type Props = {
  children: string | JSX.Element | JSX.Element[];
};

export default function DefaultLayout({ children }: Props) {
  return (
    <>
      <>
        <Header />
        <Box>{children}</Box>
        <Background />
        <Terms />
      </>
    </>
  );
}
