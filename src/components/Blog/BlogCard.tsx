import {
  Image as IMG,
  Card,
  Text,
  CardBody,
  CardFooter,
  Heading,
  Stack,
  Badge,
  HStack,
} from "@chakra-ui/react";
import { motion } from "framer-motion";
import Link from "next/link";
import { type NDBFields } from "./NotionDatabaseFields";

export const BlogCard = ({
  id,
  Name,
  Description,
  Tags,
  Image,
  Author,
  Date,
}: NDBFields) => {
  return (
    <motion.div whileHover={{ scale: 1.05 }} whileTap={{ scale: 0.95 }}>
      <Card
        variant="filled"
        as={Link}
        background={"#171923"}
        maxW="sm"
        borderRadius={15}
        h="380px"
        href={`/blog/${id}`}
      >
        <IMG
          borderRadius="15px 15px 0 0"
          maxH={"100px"}
          objectFit="cover"
          src={Image[0]?.url}
        />

        <CardBody>
          <Stack>
            <Heading fontWeight={300} size="md">
              {Name}
            </Heading>
            <HStack>
              {Tags
                ? Tags.map((tag) => {
                    return <Badge key={tag}>{tag}</Badge>;
                  })
                : ""}
            </HStack>
          </Stack>
          <Stack pt={2}>
            <Text fontSize={"xs"}>{truncateString(Description, 100)}</Text>
          </Stack>
        </CardBody>
        <CardFooter>
          <HStack>
            <IMG w={10} src={catchImage(Author)}></IMG>
            <Text pt={5} fontWeight={150} fontSize={"1xs"}>
              {Author} • {Date}
            </Text>
          </HStack>
        </CardFooter>
      </Card>
    </motion.div>
  );
};

function truncateString(str: string, num: number) {
  if (str.length <= num) {
    return str;
  }
  return str.slice(0, num) + "...";
}
function catchImage(name: string | undefined) {
  return name === "Peter"
    ? "/crew/peter.svg"
    : name === "Flo"
    ? "/crew/flo.svg"
    : name === "Kai"
    ? "/crew/kai.svg"
    : "";
}
