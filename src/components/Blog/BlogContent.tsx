/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/restrict-plus-operands */
import {
  VStack,
  Box,
  Spinner,
  Heading,
  Spacer,
  HStack,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { NotionRenderer } from "react-notion-x";
import { type ExtendedRecordMap } from "notion-types";

import axios from "axios";
// core styles shared by all of react-notion-x (required)
import defaultRecordMap from "./record-map.json";
// used for code syntax highlighting (optional)
import "prismjs/themes/prism-tomorrow.css";
// used for rendering equations (optional)
import { Code } from "react-notion-x/build/third-party/code";
import { Collection } from "react-notion-x/build/third-party/collection";
import { Equation } from "react-notion-x/build/third-party/equation";
import { Modal } from "react-notion-x/build/third-party/modal";
import { ChevronLeftIcon } from "@chakra-ui/icons";
import { motion } from "framer-motion";
import Link from "next/link";
import { getPageTitle } from "notion-utils";
import Head from "next/head";

type AppProps = {
  blog_id: string | string[];
};

export const BlogContent = ({ blog_id }: AppProps) => {
  const [blogPost, setBlogPost] = useState<ExtendedRecordMap>(defaultRecordMap);
  const [isLoading, setLoading] = useState(false);
  const page_api = "https://notion-api.splitbee.io/v1/page/";
  const recordMap = defaultRecordMap as unknown as ExtendedRecordMap;

  useEffect(() => {
    setLoading(true);
    axios
      .get(page_api + blog_id)
      .then(function (response) {
        blogPost.block = response.data;

        setLoading(false);
      })
      .catch(function (error) {
        console.log(error);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Head>
        <title>{getPageTitle(blogPost)}</title>
      </Head>
      <Box ml={5}>
        <Box position="fixed">
          <Link href="/blog" onClick={() => setBlogPost(recordMap)}>
            <motion.div whileHover={{ scale: 1.2 }} whileTap={{ scale: 0.9 }}>
              <HStack pt={4}>
                <ChevronLeftIcon fontSize={45} />
              </HStack>
            </motion.div>
          </Link>
        </Box>
      </Box>
      <VStack pt={4}>
        <Heading
          bgGradient="linear(to-l, #7928CA, #FF0080)"
          bgClip="text"
          fontSize="5xl"
          fontWeight="extrabold"
        >
          Blogpost
        </Heading>
        <Spacer p={2} />
        {isLoading ? (
          <Spinner size="xl" />
        ) : (
          <Box zIndex={100} w={{ base: "340px", md: "600px", lg: "800px" }}>
            {blogPost ? (
              <NotionRenderer
                fullPage={true}
                darkMode={true}
                recordMap={blogPost}
                components={{
                  Code,
                  Collection,
                  Equation,
                  Modal,
                }}
                disableHeader={true}
              />
            ) : (
              ""
            )}
          </Box>
        )}
      </VStack>
    </>
  );
};
