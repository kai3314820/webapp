import { Box, Stack, VStack, useColorMode } from "@chakra-ui/react";
import Link from "next/link";
import { SidebarHeader } from "./SidebarHeader";
import { SidebarItem } from "./SidebarItem";
import { useSession } from "next-auth/react";

export const Sidebar = () => {
  const { colorMode } = useColorMode();
  const { data: session } = useSession();

  return (
    <Box>
      <Stack
        h="100vh"
        bg={colorMode === "dark" ? "gray.800" : "gray.300"}
        w={["65px", "65px", "320px"]}
        justify={"space-between"}
      >
        <Box>
          <SidebarHeader />
          <VStack spacing={2} pt={20} align="left">
            <Link href={"/webapp/home"}>
              <SidebarItem iconPath="/icons/mdhome.svg" title="Home" />
            </Link>
            <Link href={"/webapp/campain-manager"}>
              <SidebarItem
                iconPath="/icons/mdcampain.svg"
                title="Campain manager"
              />
            </Link>
            <Link href={"/webapp/utility-controller"}>
              <SidebarItem
                iconPath="/icons/mdcontroller.svg"
                title="Utility controller"
              />
            </Link>

            <Link href={"/webapp/later"}>
              <SidebarItem
                iconPath="/icons/mdactivity.svg"
                title="Activity dashboard"
              />
            </Link>

            <Link href={"/webapp/later"}>
              <SidebarItem
                iconPath="/icons/mdwallet.svg"
                title="Wallet manager"
              />
            </Link>

            <Link href={"/webapp/later"}>
              <SidebarItem
                iconPath="/icons/mdintegration.svg"
                title="Integrator"
              />
            </Link>
          </VStack>
        </Box>
        {session?.user.isAdmin && (
          <Box>
            <VStack spacing={2} pb={6} align="left">
              <Link href={"/webapp/admin/users"}>
                <SidebarItem emoji="🎛️" title="Users" />
              </Link>
              <Link href={"/webapp/admin/waitlist"}>
                <SidebarItem emoji="🤼" title="Waitlist" />
              </Link>
              <Link href={"/webapp/admin/account"}>
                <SidebarItem emoji="🕺" title="Account" />
              </Link>
            </VStack>
          </Box>
        )}
      </Stack>
    </Box>
  );
};
