import { HStack, Text, Image } from "@chakra-ui/react";
import React from "react";

interface ISidebarItemProps {
  title: string;
  iconPath?: string;
  emoji?: string;
}

export const SidebarItem = ({ title, iconPath, emoji }: ISidebarItemProps) => {
  return (
    <HStack _hover={{ bg: "gray.700" }} p={2} pl={6} spacing={6}>
      {iconPath && <Image alt={`icon-${title}`} src={iconPath} />}
      {emoji && <Text fontSize={20}>{emoji}</Text>}
      <Text display={["none", "none", "flex"]} fontSize="16px" fontWeight="700">
        {title}
      </Text>
    </HStack>
  );
};
