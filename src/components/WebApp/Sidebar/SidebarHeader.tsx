import { HStack, Text, Image, Box } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";
import { AuthShowcase } from "~/components/AuthShowcase";

export const SidebarHeader = () => {
  return (
    <HStack pl={4} pt={4} justifyContent="space-between">
      <HStack pt={2} spacing={6}>
        <Link href={"/webapp"}>
          <Image alt="logo" src={"/icons/logo.svg"}></Image>
        </Link>
        <Text
          display={["none", "none", "flex"]}
          fontSize="18px"
          fontWeight="400"
        >
          BTBverse
        </Text>
      </HStack>
      <Box display={["none", "none", "flex"]} pr={4} pt={2}>
        <AuthShowcase />
      </Box>
    </HStack>
  );
};
