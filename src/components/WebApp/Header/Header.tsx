import {
  ChevronLeftIcon,
  ChevronRightIcon,
  SearchIcon,
} from "@chakra-ui/icons";
import {
  HStack,
  Button,
  Input,
  InputGroup,
  InputLeftElement,
  useColorMode,
  Flex,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import { DarkModeSwitch } from "../DarkModeSwitch";

export const Header = () => {
  const { colorMode } = useColorMode();
  const router = useRouter();

  function transformPath(path: string | undefined) {
    if (path) {
      const upper = path.charAt(0).toUpperCase() + path.slice(1);
      return upper.replace("-", " ");
    }
    return "";
  }

  return (
    <HStack
      p={8}
      h="72px"
      bg={colorMode === "dark" ? "gray.800" : "gray.300"}
      justifyContent="space-between"
    >
      <HStack display={["none", "none", "none", "flex"]}>
        <ChevronLeftIcon
          cursor="pointer"
          boxSize={8}
          onClick={() => window.history.back()}
        />
        <ChevronRightIcon
          boxSize={8}
          cursor="pointer"
          onClick={() => window.history.forward()}
        />
        <Flex>
          <Breadcrumb>
            <BreadcrumbItem>
              <BreadcrumbLink href="/webapp">Webapp</BreadcrumbLink>
            </BreadcrumbItem>
            {router.pathname.split("/")[2] && (
              <BreadcrumbItem>
                <BreadcrumbLink
                  href={`/webapp/${router.pathname.split("/")[2] || ""}`}
                >
                  {transformPath(router.pathname.split("/")[2])}
                </BreadcrumbLink>
              </BreadcrumbItem>
            )}
            {router.pathname.split("/")[3] && (
              <BreadcrumbItem>
                <BreadcrumbLink
                  href={`/webapp/admin/${router.pathname.split("/")[3] || ""}`}
                >
                  {transformPath(router.pathname.split("/")[3])}
                </BreadcrumbLink>
              </BreadcrumbItem>
            )}
          </Breadcrumb>
        </Flex>
      </HStack>
      <HStack spacing={8}>
        <InputGroup borderColor={colorMode === "dark" ? "white" : "black"}>
          <InputLeftElement>
            <SearchIcon />
          </InputLeftElement>
          <Input variant="outline" placeholder="Search" />
        </InputGroup>
        <Button display={["none", "none", "flex"]} variant="ghost">
          Developers
        </Button>
        <DarkModeSwitch />
      </HStack>
    </HStack>
  );
};
