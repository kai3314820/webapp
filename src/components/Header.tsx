import { Box, HStack, Text } from "@chakra-ui/react";
import Link from "next/link";
import { useRouter } from "next/router";
import { AuthShowcase } from "./AuthShowcase";

export const Header = () => {
  const router = useRouter();

  return (
    <>
      <HStack
        position={["unset", "fixed"]}
        top={4}
        right={4}
        spacing={4}
        p={3}
        pr={10}
        zIndex={120}
        justify={"right"}
      >
        <Box
          _hover={{ textDecoration: "underline" }}
          onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
        >
          {router.pathname === "/" ? (
            <Text cursor="pointer">Home</Text>
          ) : (
            <Link href={"/"}>Home</Link>
          )}
        </Box>
        <Text fontSize={19}>|</Text>
        <Box _hover={{ textDecoration: "underline" }}>
          <Link href={"/blog"}>Blog</Link>
        </Box>
        <AuthShowcase />
      </HStack>
    </>
  );
};
