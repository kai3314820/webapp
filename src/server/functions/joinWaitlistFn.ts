import { generateHtmlMail } from "./mailTemplate";
import nodemailer from "nodemailer";

export const sendEmail = async (email: string, token: string) => {
  const endpoint = `${
    process.env.NEXTAUTH_URL || "http://localhost:3000"
  }/confirm/${token}`;
  const mailHtml = generateHtmlMail(endpoint);
  const mailOptions = {
    from: '"BTBverse newsletter" <info@btbverse.xyz>',
    to: email,
    subject: "Please confirm your subscription",
    html: mailHtml,
  };

  const mailUser = process.env.mail_user;
  const mailClientId = process.env.mail_client_id;
  const mailClientSecret = process.env.mail_client_secret;
  const mailRefreshToken = process.env.mail_refresh_token;
  const mailAccessToken = process.env.mail_access_token;
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      type: "OAuth2",
      user: mailUser,
      clientId: mailClientId,
      clientSecret: mailClientSecret,
      refreshToken: mailRefreshToken,
      accessToken: mailAccessToken,
    },
  });
  await new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        reject(error);
      } else {
        resolve(info);
      }
    });
  });

  return email;
};
