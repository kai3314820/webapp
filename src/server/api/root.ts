import { createTRPCRouter } from "~/server/api/trpc";
import { exampleRouter } from "~/server/api/routers/example";
import { account } from "./routers/account";
import { waitlist } from "./routers/waitlist";

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here.
 */
export const appRouter = createTRPCRouter({
  example: exampleRouter,
  account: account,
  waitlist: waitlist,
});

// export type definition of API
export type AppRouter = typeof appRouter;
