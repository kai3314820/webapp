import { type Waitlist } from "@prisma/client";
import { TRPCError } from "@trpc/server";
import { randomUUID } from "crypto";
import { z } from "zod";

import { createTRPCRouter, publicProcedure } from "~/server/api/trpc";
import { prisma } from "~/server/db";
import { adminProcedure } from "../trpc";
import { sendEmail } from "../../functions/joinWaitlistFn";

export const waitlist = createTRPCRouter({
  add: publicProcedure
    .input(
      z.object({
        email: z.string().min(8),
      })
    )
    .mutation(async ({ input }) => {
      const newWaitlistUser = {
        email: input.email,
        token: randomUUID(),
      };
      const duplicate = await prisma.waitlist.findFirst({
        where: { email: input.email },
      });

      if (duplicate) {
        throw new TRPCError({
          code: "CONFLICT",
          message: "Already signd up",
        });
      }

      const waitlistUser: Waitlist | null = await prisma.waitlist.create({
        data: newWaitlistUser,
      });

      // TODO: Send Email
      await sendEmail(waitlistUser.email, waitlistUser.token);

      return { waitlistUser };
    }),

  confirm: publicProcedure
    .input(
      z.object({
        token: z.string().min(8),
      })
    )
    .mutation(async ({ input }) => {
      const user = await prisma.waitlist.findFirst({
        where: { token: input.token },
      });

      if (!user) {
        throw new TRPCError({
          code: "NOT_FOUND",
          message: "User not found",
        });
      } else if (user.emailVerified) {
        throw new TRPCError({
          code: "CONFLICT",
          message: "User already confirmed",
        });
      } else {
        const updatedUser: Waitlist | null = await prisma.waitlist.update({
          where: { id: user.id },
          data: { emailVerified: new Date() },
        });

        return { updatedUser };
      }
    }),

  listAll: adminProcedure.query(async () => {
    const wailistUser: Waitlist[] | null = await prisma.waitlist.findMany();
    return wailistUser;
  }),

  delete: adminProcedure
    .input(
      z.object({
        email: z.string().min(8),
      })
    )
    .mutation(async ({ input }) => {
      const user = await prisma.waitlist.findFirst({
        where: { email: input.email },
      });
      if (!user) {
        throw new TRPCError({
          code: "NOT_FOUND",
          message: "User not found",
        });
      }

      const deletedUser = await prisma.waitlist.delete({
        where: { id: user.id },
      });
      return deletedUser;
    }),
});
