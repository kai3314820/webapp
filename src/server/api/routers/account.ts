import { type User } from "@prisma/client";
import { TRPCError } from "@trpc/server";
import { z } from "zod";

import { createTRPCRouter, publicProcedure } from "~/server/api/trpc";
import { prisma } from "~/server/db";
import { adminProcedure } from "../trpc";

export const account = createTRPCRouter({
  getUsers: publicProcedure.query(async () => {
    const users: User[] | null = await prisma.user.findMany();
    return users;
  }),
  promote: adminProcedure
    .input(
      z.object({
        email: z.string().min(8),
      })
    )
    .mutation(async ({ input }) => {
      const user = await prisma.user.findUnique({
        where: { email: input.email },
      });
      if (!user) {
        throw new TRPCError({
          code: "NOT_FOUND",
          message: "User not found",
        });
      }

      await prisma.user.update({
        where: { id: user.id },
        data: { isAdmin: true },
      });

      const users: User[] | null = await prisma.user.findMany();

      return users;
    }),

  degrade: adminProcedure
    .input(
      z.object({
        email: z.string().min(8),
      })
    )
    .mutation(async ({ input }) => {
      const user = await prisma.user.findUnique({
        where: { email: input.email },
      });
      if (!user) {
        throw new TRPCError({
          code: "NOT_FOUND",
          message: "User not found",
        });
      }

      await prisma.user.update({
        where: { id: user.id },
        data: { isAdmin: false },
      });

      const users: User[] | null = await prisma.user.findMany();

      return users;
    }),
});
